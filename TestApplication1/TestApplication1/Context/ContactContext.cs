﻿using Microsoft.EntityFrameworkCore;
using TestApplication1.Models;

namespace TestApplication1.Context
{
    public class ContactContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<City> Cities { get; set; }
        public ContactContext(DbContextOptions<ContactContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<City>().HasData(
                new City { Id = 1, Name = "Москва" },
                new City { Id = 2, Name = "Новосибирск" },
                new City { Id = 3, Name = "Владивосток" });
        }
    }
}