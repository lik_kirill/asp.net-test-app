﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using TestApplication1.Context;
using TestApplication1.Models;

namespace TestApplication1.Controllers
{
    public class HomeController : Controller
    {
        ContactContext contactContext;
        public HomeController(ContactContext _contactContext)
        {
            contactContext = _contactContext;
        }
        public IActionResult Index()
        {
            var contact = new CommonContact()
            {
                ListCity = contactContext.Cities.Select(x => new City() { Id = x.Id, Name = x.Name }).ToList()
            };
            return View(contact);
        }
        [HttpPost]
        public IActionResult SaveContact(Contact contact)
        {
            try
            {
                contactContext.Contacts.Add(contact);
                contactContext.SaveChanges();
                return Json(new { result = true });
            }
            catch
            {
                return Json(new { result = false });
            }
        }
    }
}
