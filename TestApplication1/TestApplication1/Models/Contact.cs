﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestApplication1.Models
{
    public class Contact
    {
        private const string _emailRegex = @"^([a-zA-Z0-9!#$%&'*+\-\/=?^_`{|}~])+([\.]([a-zA-Z0-9!#$%&'*+\-\/=?^_`{|}~])+)*@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$";
        public int Id { get; set; }
        [Display(Name = "ФИО"), Required(AllowEmptyStrings = false, ErrorMessage = "ФИО: поле обязательно для заполнения")]
        [StringLength(50, ErrorMessage = "ФИО не может содержать больше 50 символов")]
        public string Name { get; set; }
        [Display(Name = "Телефон"), RegularExpression("^([0-9.\\(\\)+\\s]){16}$", ErrorMessage = "Введите номер телефона полностью")]
        public string Phone { get; set; }
        [Display(Name = "E-mail"), StringLength(36, ErrorMessage = "E-mail не может содержать больше 36 символов")]
        [RegularExpression(_emailRegex, ErrorMessage = "Введите корректный e-mail")]
        public string Email { get; set; }
        [Display(Name = "Город")]
        public int CityId { get; set; }
        public City City { get; set; }
    }
    public class CommonContact : Contact
    {
        public List<City> ListCity { get; set; }
    }
}
